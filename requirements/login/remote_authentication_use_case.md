# Remote Authentication Use Case

> ## Caso de sucesso

1. ✅ Sistema valida os dados (enviar dados para API com o formato esperado)
2. ✅ Sistema faz uma requisição para a URL da API de login
3. ✅ Sisitema valida os dados recebidos da API
4. ✅ Sisitema entrega os dados da conta da usuária

> ## Exceção - URL inválida

1. ✅ Sistema retorna uma mensagem de erro inesperado

> ## Exceção - Dados inválidos

1. ✅ Sistema retorna uma mensagem de erro inesperado

> ## Exceção - Reposta inválida

1. ✅ Sistema retorna uma mensagem de erro inesperado

> ## Exceção - Falha no servidor

1. ✅ Sistema retorna uma mensagem de erro inesperado

> ## Exceção - Credenciais inválidas

1. ✅ Sistema retorna uma mensagem de erro informando que as credenciais estão erradas
