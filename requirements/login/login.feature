Feature: Login
  Como uma cliente
  Quero poder acessar minha conta e me manter logadp
  Para que eu possa ver e responder enquetes de forma rápida

Scenario: Credenciais Válidas
  Dado que a cliente informou credenciais válidas
  Quando solicitar para fazer login
  Então o sistema deve enviar a usuária para a tela de pesquisas
  E manter a usuária conectada

Scenario: Credenciais Inválidas
  Dado que a cliente informou credenciais inválidas
  Quando solicitar para fazer login
  Então o sistema deve retornar uma mensagem de erro
